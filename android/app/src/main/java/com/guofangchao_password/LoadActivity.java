package com.guofangchao_password;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class LoadActivity extends AppCompatActivity {
    ProgressBar progressBar;
    TextView tv_update;
    ImageView iv_arrow;
    int mProgress = 0;
    long starTime;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_activity);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tv_update = (TextView) findViewById(R.id.tv_update);
        iv_arrow = (ImageView) findViewById(R.id.iv_arrow);
        starTime = System.currentTimeMillis();
        startMainActivity();
    }

    private void startMainActivity() {
        long time =System.currentTimeMillis()-starTime;
        if(time<30000){
            time=3000-time;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(LoadActivity.this, MainActivity.class));
                finish();
            }
        }, 500+time);
    }
}
