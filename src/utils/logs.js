

let socket;
let logs = [];
let debug = false;


exports.getLogs = function () {
    return logs;
}
exports.getLog = function (index) {
    return logs[index];
}

exports.log = function (...args) {
    logs.push({ msg: args.concat() })
    if (__DEV__) {
        let info = args.concat();
        info[0] = "%c" + info[0];
        info.splice(1, 0, 'color: #2d8cf0');
        console.log(...info)
    }
    if (debug) getLog(...args);
    args[1] = {};
}
exports.logWarm = function (...args) {
    logs.push({ msg: args })
    if (__DEV__) {
        let info =args.concat();
        info[0] = "%c" + info[0];
        info.splice(1, 0, 'color: #ff9900');
        console.log(...info)
    }
    if (debug) getWarm(...args)
}
exports.logErr = function (...args) {
    logs.push({ msg: args })
    if (__DEV__) {
        let info = args.concat();
        info[0] = "%c" + info[0];
        info.splice(1, 0, 'color: #ed3f14');
        console.log(...info)
    }
    getErr(...args);
}
