'use strict';

import { AsyncStorage } from 'react-native';

async function getItem(key) {
    let item = await AsyncStorage.getItem(key);
    if (!item) {
        return null;
    }
    return JSON.parse(item).v || null;
}

async function setItem(key, value) {
    let item = JSON.stringify({
        v: value
    });
    return await AsyncStorage.setItem(key, item);
}


async function removeItem(key) {
    return await AsyncStorage.removeItem(key);
}

async function  getKeys() {
    return await AsyncStorage.getAllKeys();
}
export { getItem, setItem, removeItem,getKeys }

