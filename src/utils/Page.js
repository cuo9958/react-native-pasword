/**
 * 页面基础类
 */
import React from 'react'
import {
    StyleSheet, View,
    Image, TouchableOpacity,
    Platform, Text, Modal
} from 'react-native';
import { log, logWarm } from './logs'
import { px } from './Ratio'
import { TopHeader } from '../common/Header'
import base from './Base'
import { DialogModal } from '../common/ModalView'
import toast from './Toast'

/**
 * extends title<string> 顶部的标题
 * extends pageHeader<fun> 顶部的渲染方法,非定制不建议改动
 * extends pageBody<fun> 内容的渲染方法,建议使用这个
 * extends pageFooter<fun> 底部的渲染方法,放在body平级的容器内
 * extends render<fun> React的渲染方法,非定制不建议使用
 * extends onLoad<fun> 渲染之前的方法,componentWillMount调用
 * extends onReady<fun> 渲染之后的方法,componentDidMount调用
 * extends componentWillMount<fun> React的渲染方法,非定制不建议使用
 * extends componentDidMount<fun> React的渲染方法,非定制不建议使用
 * extends update<fun> 返回的刷新方法
 * extends go<fun> 跳转方法
 */
export default class extends React.Component {
    constructor(props, state) {
        super(props)
        this.state = {
            loaded: false,
            ...state
        }
    }
    /**
     * 顶部标题
     */
    title = "";
    /**
     * 顶部渲染方法
     */
    pageHeader() {
        return <TopHeader navigation={this.props.navigation}
            title={this.title}></TopHeader>
    }
    /**
     * 内容渲染方法
     */
    pageBody() {
        return null
    }
    /**
     * 顶部渲染方法
     */
    pageFooter() {
        return null
    }
    /**
     * 默认渲染
     */
    render() {
        return <View style={{ flex: 1, backgroundColor: '#f5f3f6' }}>
            {this.pageHeader()}
            {!this.state.loaded && <View style={base.flex_middle}></View>}
            {this.state.loaded && this.pageBody()}
            {this.state.loaded && this.pageFooter()}
            <DialogModal ref="dialog" />
        </View>
    }
    /**
     * 渲染之前的方法
     */
    onLoad() { }
    /**
     * 渲染之后的方法
     */
    onReady() { }
    /**
     * 渲染组件之前的方法,
     * 尽量少用耗时操作
     */
    async componentWillMount() {
        await this.onLoad();
    }
    /**
     * 第一次渲染结束
     * 自带一个取消loading层的状态
     */
    async componentDidMount() {
        await this.onReady();
        this.setState({ loaded: true })
    }
    /**
     * 自带刷新方法
     */
    update() { }
    /**
     * 自带的页面跳转方法
     * @param {*} name 
     * @param {*} data 
     */
    go(name, data) {
        this.props.navigation.navigate(name, Object.assign({
            callback: () => {
                this.update()
            }
        }, data));
    }

    go_(name, data) {
        this.props.navigation.navigate(name, Object.assign({
        }, data));
    }
    //=============继承自index的方法=========

    /**
    * alert提示
    * @param {*} title 标题
    * @param {*} content<array> 内容
    * @param {*} success<array> 成功
    * @param {*} cancel<array> 取消
    * @param {*} success.txt 按钮标题
    * @param {*} success.click 按钮点击事件
    * 重载,(content<string|array>)
    * 重载,(title<string>,content<string|array>)
    * 重载,(title<string>,content<string|array>,success<string|object>)
    * 重载,(title<string>,content<string|array>,success<string|object>,cancel<string|object>)
    */
    $alert(title, content, success, cancel) {
        if (!title) {
            title = null
        }
        if (title && !content) {
            let t = title;
            title = content;
            content = t;
        }
        if (typeof content === "string") content = [content]
        if (typeof success === "string") success = { txt: success }
        if (typeof cancel === "string") success = { txt: cancel }
        this._alert(title, content, success, cancel)
    }
    _alert(title, content, success, cancel) {
        let opt = {
            title, content,
            btns: []
        }
        if (success) opt.btns.push(success)
        if (cancel) opt.btns.push(cancel)
        this.refs.dialog.open(opt);
    }
    /**
     * 小提示
     * @param {*} msg 
     */
    $toast(msg) {
        toast(msg);
    }
}
