'use strict';

import React from 'react';
import {
    StyleSheet,
} from 'react-native';

/**
 * 基础样式
 */
export default StyleSheet.create({
    /**
     * 一行，居中
     */
    line:{
        alignItems: 'center',
        justifyContent: 'center',
    },
    position:{
        position:'absolute',
    },
    inline:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inline_between:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    text_center:{
        alignItems: 'center',
        justifyContent: 'center',
    },
    flex_middle:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    }
})