
import {
    Dimensions,
    Platform,
    PixelRatio
} from 'react-native';


const deviceWidth = Dimensions.get('window').width;

export function px(size) {
    if (PixelRatio.get() >= 3 && Platform.OS == 'ios' && size == 1) {
        return size;
    }
    return deviceWidth / 750 * size;
}
export function getSize(size) {
    if (PixelRatio.get() >= 3 && Platform.OS == 'ios' && size == 1) {
        return size;
    }
    return 750 / deviceWidth * size;
}