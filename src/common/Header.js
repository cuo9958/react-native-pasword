'use strict';

import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Platform,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Animated,
} from 'react-native';
import { px } from '../utils/Ratio';

/**
 * 头部组件，
 * boxStyles,外层样式
 * props title<string> 标题
 * props navigation
 * props leftBtn
 * props rightBtn 右边按钮
 */
exports.TopHeader = class extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <View style={topStyles.header}>
            {Platform.OS == 'ios' && <View style={topStyles.topBox}></View>}
            <View style={[topStyles.bar, this.props.boxStyles]}>
                <View style={topStyles.leftBtn}>
                    <TouchableOpacity style={topStyles.back} onPress={() => {
                        (this.props.navigation.state.params || {}).callback && this.props.navigation.state.params.callback();
                        this.props.navigation.goBack()
                    }}>
                        <View style={{ height: px(60), justifyContent: "center" }}>
                            <Image source={{ uri: require('../images/icon-back') }}
                                style={{ width: px(40), height: px(40) }} />
                        </View>
                    </TouchableOpacity>
                    {this.props.leftBtn}
                </View>
                <Text style={topStyles.title}>{this.props.title}</Text>
                <View style={topStyles.rightBtn}>
                    {this.props.rightBtn}
                </View>
            </View>
        </View>
    }
}
const topStyles = StyleSheet.create({
    header: {
        backgroundColor: '#fbfafc',
        width: px(750)
    },
    topBox: {
        width: px(750),
        height: px(30)
    },
    bar: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        borderBottomColor: "#ddd",
        borderBottomWidth: 1,
        height: px(90),
        paddingTop: px(10)
    },
    title: {
        flex: 1,
        fontSize: px(30),
        textAlign: "center",
        color: "#252426"
    },
    leftBtn: {
        flexDirection: "row",
        justifyContent: "flex-start",
        width: px(120),
    },
    rightBtn: {
        width: px(120),
        flexDirection: "row",
        justifyContent: "flex-end",
    },
    back: {
        marginLeft: px(10)
    },
})
