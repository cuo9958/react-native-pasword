'use strict';

import React, { PureComponent } from 'react';
import {
    Modal, Text, View, StyleSheet,
    TouchableOpacity, Image,
    TouchableWithoutFeedback, TextInput,
    Dimensions, Animated,
    Platform
} from 'react-native'
import { px } from '../utils/Ratio';
import toast from '../utils/Toast'
import { log, logErr, logWarm } from '../utils/logs'
import base from '../utils/Base'

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

/**
 * 传入列表，展示pop菜单
 * props list<Object>{title,onPress} 传入的菜单项
 */
exports.PopModal = class extends React.Component {
    constructor(props) {
        super(props);
        this.height = px(115)
        if (this.props.list) {
            this.height = px(115 + this.props.list.length * 100)
        }
        this.state = {
            showModal: false,
            boxY: new Animated.Value(this.height),
        };
    }
    render() {
        return <Modal
            visible={this.state.showModal}
            onShow={() => { }}
            onRequestClose={() => { }}
            animationType="none"
            transparent={true}>
            <View style={styles.view}>
                <TouchableWithoutFeedback onPress={() => this.cancel()}><View style={styles.bg} ></View></TouchableWithoutFeedback>
                <Animated.View style={[popStyles.box, {
                    transform: [
                        { translateY: this.state.boxY }
                    ]
                }]}>
                    {this.props.list && this.props.list.map((item, index) => <TouchableOpacity key={index} onPress={() => {
                        item.onPress && item.onPress(); this.cancel()
                    }} activeOpacity={0.5}>
                        <View style={[base.line, popStyles.btn]} >
                            <Text>{item.title}</Text>
                        </View>
                    </TouchableOpacity>)}
                    <TouchableOpacity onPress={() => this.cancel()} activeOpacity={0.5}>
                        <View style={[base.line, popStyles.btn, popStyles.cancel]} >
                            <Text>取消</Text>
                        </View>
                    </TouchableOpacity>
                </Animated.View>
            </View>
        </Modal>
    }

    cancel() {
        Animated.timing(
            this.state.boxY,
            {
                toValue: this.height,
                duration: 200
            }
        ).start(() => {
            this.setState({
                showModal: false
            })
        });
    }
    Open() {
        if (!this.state.showModal) {
            this.setState({
                showModal: true
            })
            Animated.timing(
                this.state.boxY,
                {
                    toValue: 0,
                    duration: 200
                }
            ).start();
        }
    }
}
const popStyles = StyleSheet.create({
    box: {
        paddingTop: px(1),
        backgroundColor: '#eee',
        overflow: 'hidden',
    },
    boxbg: {
        flex: 1,
        position: 'absolute',
        width: deviceWidth,
        zIndex: 10
    },
    btn: {
        paddingVertical: px(30),
        backgroundColor: '#fff',
        borderBottomWidth: px(2),
        borderColor: '#eee',
        height: px(100)
    },
    btnTxt: {
        fontSize: px(34)
    },
    cancel: {
        marginTop: px(15)
    }
})



/**
 * alert提示
 * @param {*} opt.title 标题
 * @param {*} opt.content<array> 内容
 * @param {*} opt.btns<array> 按钮组
 * @param {*} opt.btns.txt 按钮标题
 * @param {*} opt.btns.click 按钮点击事件
 */
exports.DialogModal = class extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            opt: { title: "", content: [], btns: [] }
        }
    }
    render() {
        let opt = this.state.opt;
        return <Modal
            visible={this.state.show}
            onShow={() => { }}
            onRequestClose={() => { }}
            animationType="none"
            transparent={true}>
            {this.state.show && <View style={[base.flex_middle, { backgroundColor: "rgba(0,0,0,.5)" }]}>
                <View style={DialogStyle.alert_box}>
                    <View style={DialogStyle.alert_title}>
                        {opt.title && <Text style={DialogStyle.alert_title_txt}>{opt.title}</Text>}
                    </View>
                    <View style={DialogStyle.alert_body}>
                        {opt.content.map((txt, index) =>
                            <Text key={index} style={DialogStyle.alert_body_txt}>{txt}</Text>
                        )}
                    </View>
                    <View style={DialogStyle.alert_foot}>
                        {opt.btns.map((btn, index) => <TouchableOpacity key={index} onPress={() => {
                            btn.click && btn.click(); this.setState({ show: false })
                        }}><View style={[DialogStyle.alert_foot_btn, { borderLeftWidth: index > 0 ? px(1) : 0 }]} ><Text style={{ color: btn.color }}>{btn.txt}</Text></View></TouchableOpacity>
                        )}
                    </View>
                </View>
            </View>}
        </Modal>
    }
    open(opt) {
        if (!opt || !opt.content) return logWarm("alert没有传入内容参数");
        if (!opt.btns || opt.btns.length === 0) {
            opt.btns = [{ txt: "确定", click: () => { } }]
        }
        if (opt.btns.length == 1) {
            opt.btns[0].color = "#d0648f";
        }
        if (opt.btns.length == 2) {
            opt.btns[1].color = "#d0648f";
        }
        this.setState({
            show: true, opt
        })
    }
}
const DialogStyle = StyleSheet.create({
    alert_box: {
        width: px(600),
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: px(25),
        overflow: "hidden"
    },
    alert_title: {
        paddingTop: px(40),
    },
    alert_title_txt: {
        fontSize: px(34),
    },
    alert_body: {
        paddingHorizontal: px(20),
        paddingTop: px(5),
        paddingBottom: px(30),
        alignItems: 'center',
    },
    alert_body_txt: {
        lineHeight: px(40),
        fontSize: px(26),
    },
    alert_foot: {
        borderTopWidth: px(1),
        borderColor: "#ccc",
        width: px(600),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    alert_foot_btn: {
        width: px(300),
        alignItems: 'center',
        borderColor: "#ccc",
        paddingVertical: px(30)
    }
})