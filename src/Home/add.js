
import React from 'react'
import {
    View
} from 'react-native'
import {
    Header,
    FormLabel, FormInput,
    Button
} from 'react-native-elements'

import { setItem, getItem, removeItem } from '../utils/Storage'
import toast from '../utils/Toast'
import request from '../request'

export default class extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.navigation.state.params ? this.props.navigation.state.params.id : 0,
            title: "",
            additional: "",
            content: "",
            pwd: "",
            remark: ""
        }
    }
    render() {
        return <View style={{ flex: 1 }}>
            <Header
                leftComponent={{ icon: 'arrow-back', onPress: () => this.props.navigation.goBack(), color: '#fff' }}
                centerComponent={{ text: '添加一个密码', style: { color: '#fff' } }}
            />
            <View>
                <FormInput autoCapitalize="none" value={this.state.title}
                    placeholder="请输入名称/url" onChangeText={(e) => this.setState({ title: e })} />
                <FormInput autoCapitalize="none" placeholder="请输入账户名称"
                    value={this.state.additional} onChangeText={(e) => this.setState({ additional: e })} />
                <FormInput autoCapitalize="none" placeholder="请输入内容" multiline
                    containerStyle={{ height: 60 }}
                    value={this.state.content} onChangeText={(e) => this.setState({ content: e })} />
                <FormInput autoCapitalize="none" placeholder="请输入密码"
                    value={this.state.pwd} onChangeText={(e) => this.setState({ pwd: e })} />
                <FormInput autoCapitalize="none" multiline placeholder="请输入备注"
                    containerStyle={{ height: 60 }} value={this.state.remark} onChangeText={(e) => this.setState({ remark: e })} />
            </View>
            <Button
                textStyle={{ color: "#fff" }}
                onPress={() => this.add()}
                buttonStyle={{ backgroundColor: "rgba(92, 99,216, 1)", marginTop: 5 }}
                title="添加新账户密码" />
            <Button
                textStyle={{ color: "#fff" }}
                onPress={() => this.update()}
                buttonStyle={{ backgroundColor: "rgba(92, 99,216, 1)", marginTop: 5 }}
                title="更新账户信息" />
            {this.state.id > 0 &&
                <Button
                    textStyle={{ color: "#fff" }}
                    onPress={() => this.del()}
                    buttonStyle={{ backgroundColor: "rgba(92, 99,216, 1)", marginTop: 5 }}
                    title="删除" />
            }
        </View>
    }
    async componentDidMount() {
        if (this.state.id > 0) {
            try {
                let obj = await request.post("/select", { id: this.state.id });
                this.setState(obj)
            } catch (e) {
                toast("不存在的密码")
            }
        }
    }
    del() {
        let id = this.state.id;
        try {
            request.post("/del", { id: this.state.id })
            toast("删除")
        } catch (e) {
            toast("删除失败")
        }
    }
    update() {
        let id = this.state.id;
        try {
            request.post("/data", this.state)
            toast("更新成功")
        } catch (e) {
            toast("更新失败")
        }
    }
    add() {
        let id = this.state.id;
        try {
            request.post("/data", this.state)
            toast("添加成功")
        } catch (e) {
            toast("添加失败")
        }
    }
}