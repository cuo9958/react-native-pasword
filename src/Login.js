/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native';
import { TextInputLayout } from 'rn-textinputlayout';
import Page from './utils/Page'
import px from './utils/px'
import request from './request'
import toast from './utils/Toast'
import { setItem } from './utils/Storage'
import { NavigationActions } from 'react-navigation';

export default class extends Page {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            pwd: ""
        }
    }
    title = "登录"
    pageBody() {
        return <View style={styles.container}>
            <View style={styles.row3}>
                <Text style={styles.logo}>密码本</Text>
            </View>
            <View style={styles.row}>
                <TextInputLayout style={styles.layout}>
                    <TextInput onChangeText={t => this.setState({ username: t })}
                        autoCapitalize="none"
                        autoCorrect={false}
                        style={styles.text} placeholder="请输入用户名" />
                </TextInputLayout>
            </View>
            <View style={styles.row}>
                <TextInputLayout style={styles.layout}>
                    <TextInput onChangeText={t => this.setState({ pwd: t })}
                        autoCapitalize="none"
                        autoCorrect={false}
                        secureTextEntry={true}
                        style={styles.text} placeholder="请输入密码" />
                </TextInputLayout>
            </View>
            <TouchableOpacity onPress={() => this.login()} activeOpacity={0.8}>
                <View style={styles.row2}>
                    <Text style={styles.login}>登录</Text>
                </View>
            </TouchableOpacity>
            <View style={styles.row4}>
                <Text style={styles.info}>数据全部加密处理</Text>
            </View>
        </View>
    }
    //登录
    async login() {
        let username = this.state.username;
        let pwd = this.state.pwd;

        try {
            let res = await request.post('/login', { username, pwd })
            console.log(res)
            await setItem("token", res.user)
            toast("登录成功")
            this.props.navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Home' })
                ]
            }))
        } catch (e) {
            toast("登录失败")
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    layout: {
        width: px(600),
        height: px(90),
        marginTop: px(40),
    },
    row: {
        width: px(600),
        height: px(90),
        marginBottom: px(40),
    },
    text: {
        width: px(600),
        height: px(50),
        fontSize: px(28),
    },
    row2: {
        width: px(600),
        height: px(70),
        marginTop: px(50),
        alignItems: 'center',
        justifyContent: "center",
        backgroundColor: "#2d8cf0"
    },
    login: {
        fontSize: px(28),
        color: "#fff"
    },
    row3: {
        width: px(600),
        height: px(200),
        alignItems: 'center',
        justifyContent: "center",
    },
    logo: {
        fontSize: px(50),
    },
    row4: {
        width: px(600),
        height: px(190),
        alignItems: 'flex-end',
        justifyContent: "flex-start",
        marginTop: px(20)
    },
    info: {
        fontSize: px(24),
        color: "#ccc"
    },
});
