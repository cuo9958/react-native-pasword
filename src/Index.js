'use strict';

import React from 'react';
import {
    StyleSheet,
    Platform,
    BackHandler
} from 'react-native';
import Navigation from 'react-navigation';
import toast from './utils/Toast'

//=========page=========
import Login from './Login'
import Home from './Home/index'
import Add from './Home/add'
//======================
// const Tab = Navigation.TabNavigator({

// }, {});

const Pages = Navigation.StackNavigator({
    "Login": {
        screen: Login
    },
    "Home": {
        screen: Home
    },
    "Add":{
        screen:Add
    }
}, {
        initialRouteName: "Home",
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
                const translateX = position.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [layout.initWidth, 0, 0]
                });
                const opacity = position.interpolate({
                    inputRange: [index - 1, index - 0.99, index, index + 0.99, index + 1],
                    outputRange: [0, 1, 1, 0.3, 0]
                });
                return { opacity, transform: [{ translateX }] };
            }
        }),
        navigationOptions: {
            header: null
        },
    })

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.isCanBack = true;
    }
    render() {
        return <Pages onNavigationStateChange={this.listenChange.bind(this)}></Pages>;
    }
    listenChange(state1, state2, action) {
        // log(state1);
        // log(state2)
        // log(action)
        if (action.routeName) {
            // addPath(action.routeName);
        }
        if (action.type == 'Navigation/RESET' || action.type == 'Navigation/BACK') {
            if (state2.index == 0 && state2.routes.length == 1) {
                this.isCanBack = true;
            } else {
                this.isCanBack = false;
            }
        } else {
            this.isCanBack = false;
        }
    }
    componentWillMount() {
        this.wgo = this.backDesk.bind(this);
        this.backDate = Date.now();
        BackHandler.addEventListener('hardwareBackPress', this.wgo);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.wgo);
    }
    backDesk() {
        if (!this.isCanBack) return false;
        const now = Date.now();
        if (now - this.backDate < 1000) {
            return false;
        }
        toast('再点一次返回键退出达令家');
        this.backDate = now;
        return true;
    }
}