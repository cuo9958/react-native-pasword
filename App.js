/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import RSAKey from 'react-native-rsa';
import toast from './src/utils/Toast'

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
        'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Welcome to React Native!
        </Text>
                <Text style={styles.instructions}>
                    To get started, edit App.js
        </Text>
                <Text style={styles.instructions}>
                    {instructions}
                </Text>
            </View>
        );
    }
    componentDidMount() {
        // const bits = 1024;
        // const exponent = '10001'; // must be a string. This is hex string. decimal = 65537
        // var rsa = new RSAKey();
        // rsa.generate(bits, exponent);
        // var publicKey = rsa.getPublicString(); // return json encoded string
        // var privateKey = rsa.getPrivateString();
        // console.log(publicKey, privateKey)
        // console.log(JSON.parse(publicKey))
        // rsa.setPublicString(publicKey);
        // var originText = 'sample String Value';
        // var encrypted = rsa.encrypt(originText);
        // console.log(encrypted)
        // rsa.setPrivateString(privateKey);
        // var decrypted = rsa.decrypt(encrypted);
        // console.log(decrypted)
        toast("测试")
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
